#include <iostream>
#include <vector>
#include <cmath>

#include "omp.h"
#include "interoperability_dco_boost_interval.hpp"
#include "glopt_solver.hpp"

template <typename T>
  using boost_interval_transc_t = boost::numeric::interval
  <T,
   boost::numeric::interval_lib::policies<
     boost::numeric::interval_lib::save_state
     <
       boost::numeric::interval_lib::rounded_transc_std<T>
     >,
     boost::numeric::interval_lib::checking_base<T>
   >
  >;

using namespace std;

//****************************** main ******************************//
int main(int argc, char* argv[]) {
  using interval_type = boost_interval_transc_t<double>;
  cout.precision(15);

  //  auto objective = []<typename T, typename T2>(const vector<T>& x, const vector<T2>& v) -> T
  auto objective = [](const auto& x, const auto& v) -> auto
  {
    return pow(x[0]-v[0],2)+sin(4.0*x[0]);
  };

  double YL = -8.0;
  double YU = 12.0;

  vector<interval_type> y(1);
  for (size_t i=0; i<y.size(); i++) {
    y[i] = interval_type(YL,YU);
  }

  vector<double> x(1,1.0);
  vector<vector<interval_type> > convex_regions;

  glopt_solver solver(objective,argc,argv,y,x);
  solver.config().set_val_check();
  solver.config().set_grad_check(2);
  solver.config().set_hes_check();

  solver.config().set_domain(y);
  solver.config().set_param(x);
  solver.run();
  convex_regions = solver.minima();

  for (auto& cr : convex_regions) {
    cout << cr[0] << endl;
  }

  return 0;
}
