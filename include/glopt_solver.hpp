#pragma once
#ifndef DCO_NO_ZERO_EDGE_CHECK
  #define DCO_NO_ZERO_EDGE_CHECK
#endif
#ifdef MCPP_ADJM
  #include "interoperability_dco_mcpp.hpp"
#endif
#include <fenv.h>
#include "interoperability_dco_boost_interval.hpp"
#include <vector>
#include <bitset>
#include "omp.h"
#include "glopt_solver_config.hpp"
#include "glopt_io.hpp"

using namespace std;

template<typename float_type, typename interval_type, typename objective_type>
class glopt_solver {
  objective_type& _objective;
  solver_config<interval_type,float_type> *_solver_config;
  solver_status<float_type> *_solver_status;
  vector<vector<interval_type> > _minima;
  typedef typename interval_type::traits_type::checking checking;
public:
  glopt_solver(objective_type& objective, int argc, char **argv)
    : _objective(objective)
    , _solver_config(new solver_config<interval_type,float_type>(argc,argv))
    , _solver_status(new solver_status<float_type>())
  {
    feenableexcept(FE_INVALID);
    _solver_config->print();
  }
  glopt_solver(objective_type& objective, int argc, char **argv, vector<interval_type> domain)
    : _objective(objective)
    , _solver_config(new solver_config<interval_type,float_type>(argc,argv,domain))
    , _solver_status(new solver_status<float_type>())
  {}
  glopt_solver(objective_type& objective, int argc, char **argv, vector<interval_type> domain, vector<float_type> param)
    : _objective(objective)
    , _solver_config(new solver_config<interval_type,float_type>(argc,argv,domain,param))
    , _solver_status(new solver_status<float_type>())
  {}
  void run() {
    for(size_t nt=0; nt<_solver_config->ntime(); nt++) {
#pragma omp parallel
      {
#pragma omp single
        {
          push_task(_solver_config->domain(), _solver_config->param());
#pragma omp taskwait
        }
      }
    }
    if (_solver_config->print_mode()) {
#pragma omp parallel for
      for (int proc=0; proc<omp_get_num_threads(); proc++) {
        postprocessing_file(proc,_solver_status->global_min_bound(0),_solver_config->domain().size(), "glmin");
      }
    }
  }

  void print_solver_status() { if (!_solver_config->timing_mode()) _solver_status->print(); }
  void print_solver_config() { if (!_solver_config->timing_mode()) _solver_config->print(); }

  solver_config<interval_type,float_type>& config() { return *_solver_config; }
  vector<vector<interval_type> >& minima() { return _minima; }
private:
  float_type dist(const interval_type &a, const interval_type &b) {
    return max(abs(a.lower()-b.lower()),abs(a.upper()-b.upper()));
  }

  template <typename T>
  void value(const vector<T> &x, const vector<float_type> &v, T &y) {
    y = _objective(x,v);
  }

  // Computes gradient with interval type and call check routine
  // If interpret_only is true, tape is not recorded
  // returns adjoint vector of all intermediates
  template<typename T>
  T* gradient(const vector<T> &x, const vector<float_type> &v, T& y, vector<T> &dfdx, const bool &interpret_only = false, const T& seed = T(1.0)) {
    typedef typename dco::ga1sm<T> MODE;
    typedef typename MODE::type TYPE;
    typedef typename MODE::tape_t TAPE_T;
    static thread_local TAPE_T *tape = nullptr;

    if (!interpret_only) {
      if (tape == nullptr) tape = TAPE_T::create();
      tape->reset();
      vector<TYPE> active_x(x.size());
      TYPE active_y;
      for (size_t i=0; i<x.size(); i++) {
        active_x[i] = x[i];
      }
      tape->register_variable(active_x.begin(),active_x.end());
      value(active_x,v,active_y);
      y = dco::value(active_y);
      if (_solver_status->tapesize() == 0) {
        _solver_status->set_tapesize(dco::tape_index(active_y));
      }
    } else {
      assert(tape);
      tape->zero_adjoints();
    }
    tape->_adjoint(_solver_status->tapesize()) = seed;
    tape->interpret_adjoint();

    for (size_t i=0; i<x.size(); i++) {
      dfdx[i] = tape->_adjoint(i+1);
    }
    return &(tape->_adjoint(0));
  }

  // Computes function value with interval type and call check routine
  void value_nie(const vector<interval_type> &x, const vector<float_type> &v, interval_type &y) {
    value(x,v,y);
  }

  void gradient_nie(const vector<interval_type> &x, const vector<float_type> &v, vector<interval_type> &dfdx) {
    interval_type y_tmp;
    gradient(x,v,y_tmp,dfdx);
  }

  // full Hessian
  template<typename T>
  void hessian_nie(const vector<T> &x, const vector<float_type> &v, vector<vector<T>> &d2fdx2) {
    const int v_size = 8;
    typedef typename dco::gt1v<T, 8>::type BASETYPE;
    typedef typename dco::ga1sm<BASETYPE> MODE;
    typedef typename MODE::type TYPE;
    typedef typename MODE::tape_t TAPE_T;

    static thread_local TAPE_T *tape = nullptr;
    if (tape == nullptr) tape = TAPE_T::create();

    for (size_t l=0; l<x.size()/v_size+1; l++) {
      tape->reset();

      vector<TYPE> active_x(x.size());
      for (size_t i=0; i<x.size(); i++) {
        active_x[i] = x[i];
        tape->register_variable(active_x[i]);
        if (i>=l*v_size && i<(l+1)*v_size) {
          dco::derivative(dco::value(active_x[i]))[i%v_size] = 1.0;
        }
      }

      TYPE active_y;
      value(active_x,v,active_y);
      //y = dco::value(dco::value(active_y));
      dco::value(dco::derivative(active_y)) = 1.0;
      tape->interpret_adjoint();

      for (size_t i=0; i<x.size(); i++) {
        for (size_t j=0; j<v_size && j<x.size(); j++) {
          d2fdx2[i][l*v_size+j] = dco::derivative(dco::derivative(active_x[i]))[j];
        }
      }
    }
    TAPE_T::remove(tape);
  }

  // Computes interval valued product of Hessian with seed vector to check if Hessian is not spd
  // returns d2fdx2=H*seed
  template<typename T>
  typename dco::gt1s<T>::type* hessian_vector_nie(const vector<T> &x, const vector<float_type> &v, const vector<T> &seed, vector<T> &d2fdx2) {
    typedef typename dco::gt1s<T>::type BASETYPE;
    typedef typename dco::ga1sm<BASETYPE> MODE;
    typedef typename MODE::type TYPE;
    typedef typename MODE::tape_t TAPE_T;

    static thread_local TAPE_T *tape = nullptr;
    if (tape == nullptr) tape = TAPE_T::create();
    tape->reset();

    vector<TYPE> active_x(x.size());
    for (size_t i=0; i<x.size(); i++) {
      active_x[i] = x[i];
      tape->register_variable(active_x[i]);
      dco::derivative(dco::value(active_x[i])) = seed[i];
    }

    TYPE active_y;
    value(active_x,v,active_y);
    //y = dco::value(dco::value(active_y));
    dco::value(dco::derivative(active_y)) = 1.0;
    tape->interpret_adjoint();

    for (size_t i=0; i<x.size(); i++) {
      d2fdx2[i] = dco::derivative(dco::derivative(active_x[i]));
    }

    return &(tape->_adjoint(0));
  }

  // Generate new instaces of problem with subdomains of x only splitting those that are marked in the x_mask
  // Return if any instance was generated
  bool branching(const vector<interval_type> &x, const vector<bool> &x_mask, const vector<float_type> &v, const size_t prio=0) {
    bool task_generated = false;
    size_t n_split = 0;
    vector<interval_type> new_domain(x);
    vector< vector<interval_type> > split_space(x.size(),vector<interval_type>(2));
    for (size_t i=0; i<x.size(); i++) {
      if (x_mask[i]) {
        n_split++;
        split_space[i][0].set(x[i].lower(),median(x[i]));
        split_space[i][1].set(median(x[i]),x[i].upper());
      } else {
        split_space[i].resize(1);
        new_domain[i] = x[i];
      }
    }
    assert(n_split <= x.size());
    if (n_split > 0) {
      task_generated = true;
      for (size_t c=0; c<pow(2,n_split); c++) {
        bitset<64> split_comb(c);
        size_t i_split = 0;
        for(size_t i=0; i<x.size(); i++) {
          if (x_mask[i]) {
            new_domain[i] = split_space[i][split_comb[i_split]];
            i_split++;
          }
        }
        push_task(new_domain,v,prio);
      }
    }
    if (task_generated) {
      increment_counter(7);
    }
    return task_generated;
  }

  // Computes value of objective at midpoint of domain in real arithmetic
  void value_at_midpoint(const vector<interval_type> &x, const vector<float_type> &v, float_type &y) {
    vector<float_type> x_mid(x.size());
    for (size_t i=0; i<x.size(); i++) {
      x_mid[i] = median(x[i]);
    }
    value(x_mid,v,y);
  }

  // Computes gradient of objective at midpoint of domain in real arithmetic
  float_type* gradient_at_midpoint(const vector<interval_type> &x, const vector<float_type> &v, vector<float_type> &dfdx) {
    float_type y_mid_tmp;
    vector<float_type> x_mid(x.size());
    for (size_t i=0; i<x.size(); i++) {
      x_mid[i] = median(x[i]);
    }
    return gradient(x_mid,v,y_mid_tmp,dfdx);
  }

  // Performs gradient descent steps in real arithmetic starting from midpoint
  void gradient_descent(const vector<interval_type> &x, const vector<float_type> &v, float_type &y, const size_t &n_steps=3) {
    typedef typename dco::ga1sm<float_type> MODE;
    typedef typename MODE::type TYPE;
    typedef typename MODE::tape_t TAPE_T;

    static thread_local TAPE_T *tape_float = nullptr;
    if (tape_float == nullptr) tape_float = TAPE_T::create();
    tape_float->reset();
    vector<TYPE> x_float(x.size(),0);
    vector<float_type> x_float_old(x.size(),0);
    vector<float_type> dfdx_old(x.size(),0);
    for (size_t i=0; i<x.size(); i++) {
      dco::value(x_float[i]) = median(x[i]);;
    }
    // step size alpha
    float_type alpha = 0.5;
    for (size_t j=0; j<n_steps; j++) {
      tape_float->register_variable(x_float.begin(),x_float.end());
      TYPE y_float;
      value(x_float,v,y_float);
      if (j==0) {
        y = dco::value(y_float);
      }
      dco::derivative(y_float) = 1.0;
      tape_float->interpret_adjoint();
      // descent direction p
      vector<float_type> p(x.size());
      for (size_t i=0; i<x.size(); i++) {
        p[i] = -dco::derivative(x_float[i]);
      }
      //tape_float->reset();
      float_type sum = 0, norm = 0;
      float_type x_diff, dfdx_diff;
      for (size_t i=0; i<x.size(); i++) {
        dco::value(x_float[i]) = dco::value(x_float[i]) + alpha*p[i];
        if (dco::value(x_float[i]) < x[i].lower()) dco::value(x_float[i]) = x[i].lower();
        if (dco::value(x_float[i]) > x[i].upper()) dco::value(x_float[i]) = x[i].upper();
        x_diff = dco::value(x_float[i])-x_float_old[i];
        dfdx_diff = p[i]-dfdx_old[i];
        sum += x_diff*dfdx_diff;
        norm += pow(dfdx_diff,2);
        x_float_old[i] = dco::value(x_float[i]);
        dfdx_old[i] = p[i];
      }
      if (j > 0) {
        if (norm != 0)
          alpha = abs(sum)/norm;
        else
          alpha = 0;
      }
      value(x_float,v,y_float);
      y = min(y,dco::value(y_float));
      if (alpha < _solver_config->eps_dfdx()) break;
    }
    tape_float->reset();
  }

  // Computes function value with interval type and mark problem for elimination
  // if it can't contain global minimum
  // Function evaluations in real arithemtic to find new global minimum
  // Update global minimum
  void value_check(bitset<3> &status, const vector<interval_type> &x, const vector<bool> &x_mask, const vector<float_type> &v, interval_type &y, float_type &y_float) {
    float_type max_width = 0;
    for (size_t i=0; i<x.size(); i++) {
      if (x_mask[i]) {
        max_width = max(max_width, width(x[i]));
      }
    }

    switch(_solver_config->globalization_y()) {
      default:
      case interval_natural:
        value_nie(x,v,y);
        break;
    }
    set_nan_to_inf(y);

    y_float = numeric_limits<float_type>::max();
    if (y.lower() > _solver_status->global_min_bound(0)) {
      increment_counter(1);
      status.set(0);
      return;
    } else {
      float_type y_value_check = numeric_limits<float_type>::max();
      size_t n_steps = 0;
      switch (_solver_config->value_check()) {
        case val_check_off: break;
        case upperbound: checking::is_nan(y.upper()); y_value_check = y.upper(); break;
        default:
        case midpoint: value_at_midpoint(x,v,y_value_check); break;
        case gradient_descent_3:
          n_steps = 3;
          gradient_descent(x,v,y_value_check,n_steps);
          break;
        case gradient_descent_10:
          n_steps = 10;
          gradient_descent(x,v,y_value_check,n_steps);
          break;
        case gradient_descent_log:
          n_steps = log(width(x[0])/_solver_config->eps_x());
          gradient_descent(x,v,y_value_check,n_steps);
          break;
      }
      if (y_value_check < _solver_status->global_min_bound(0)) {
        set_opt(y_value_check);
      }
      y_float = y_value_check;
    }
  }

  // Computes gradient
  // Checks whether 0 is contained in every element of interval-valued gradient
  void gradient_check(bitset<3> &status, const vector<interval_type> &x, const vector<bool> &x_mask, const vector<float_type> &v, vector<interval_type> &dfdx) {
    float_type max_width = 0;
    for (size_t i=0; i<x.size(); i++) {
      if (x_mask[i]) {
        max_width = max(max_width, width(x[i]));
      }
    }
    switch(_solver_config->globalization_dfdx()) {
      default:
      case interval_natural:
        gradient_nie(x,v,dfdx);
        break;
    }
    set_nan_to_inf(dfdx);

    switch (_solver_config->gradient_check()) {
      case grad_check_off: break;
      default:
      case stationary_only:
        for (auto const& dfdxi : dfdx) {
          if (dfdxi.lower() > 0.0 || dfdxi.upper() < 0.0) {
            if (status.none()) {
              increment_counter(2);
            }
            status.set(1);
            return;
          }
        }
        break;
    }
  }

  // Checks whether zHz violates spd
  void hessian_check(bitset<3> &status, const vector<interval_type> &x, const vector<float_type> &v, interval_type &det) {
//    vector<interval_type> z(x.size());
//    for (size_t i=0; i<x.size(); i++) {
//      z[i] = -1+2*float_type(rand())/RAND_MAX;
//    }
//    vector<interval_type> Hz(x.size());
//    hessian_vector_nie(x,v,z,Hz);
//    //interval_type det = 0;
//    for (size_t i=0; i<z.size(); i++) {
//    // det: zHz
//      det += Hz[i] * z[i];
//    }

    vector<vector<interval_type>> H(x.size(), vector<interval_type>(x.size(),0.0));
    hessian_nie(x,v,H);

    if (x.size() == 1) {
      det = H[0][0];
    } else if (x.size() == 2) {
      det = H[0][0]*H[1][1] - H[0][1]*H[1][0];
    } else if (x.size() == 3) {
      det = H[0][0]*H[1][1]*H[2][2] + H[0][1]*H[1][2]*H[2][0] + H[0][2]*H[1][0]*H[2][1]
          - H[0][2]*H[1][1]*H[2][0] - H[0][0]*H[1][2]*H[2][1] - H[0][1]*H[1][0]*H[2][2];
    } else {
      assert(x.size()<=3);
    }

    if (det.upper() < 0.0) {
      if (status.none()) {
        increment_counter(3);
      }
      status.set(2);
      return;
    }
  }

  inline void push_task(const vector<interval_type> &x, const vector<float_type> &v, const size_t prio=0) {
#pragma omp task firstprivate(x,v) priority(prio)
    {
      task(x,v);
    }
  }

  inline void increment_counter(size_t i) {
#pragma omp atomic
    _solver_status->counter(i)++;
  }

  inline void set_opt(const float_type &y_opt_new) {
#pragma omp critical
    {
      if (y_opt_new < _solver_status->global_min_bound(0)) {
        _solver_status->global_min_bound(0) = y_opt_new;
      }
    }
  }

  inline void set_nan_to_pos_inf(float_type &v) {
    if (checking::is_nan(v)) v = checking::pos_inf();
  }

  inline void set_nan_to_neg_inf(float_type &v) {
    if (checking::is_nan(v)) v = checking::neg_inf();
  }

  inline void set_nan_to_inf(interval_type &v) {
    if (checking::is_nan(v.lower())) v.set(checking::neg_inf(), v.upper());
    if (checking::is_nan(v.upper())) v.set(v.lower(), checking::pos_inf());
  }

  inline void set_nan_to_inf(vector<interval_type> &v) {
    for (auto& vi : v) {
      set_nan_to_inf(vi);
    }
  }

  // If an exception is thrown e.g. due to undefined comparisons in interval arithemtic
  // domain is refined
  void exception(const vector<interval_type> &x, const vector<bool> &x_mask, const vector<float_type> &v) {
    for (size_t i=0; i<x.size(); i++) {
      if (x_mask[i] && width(x[i]) > _solver_config->eps_ns()) {
        bool task_generated = branching(x,x_mask,v);
        if (task_generated) return;
        break;
      }
    }
    increment_counter(4);
    if (_solver_config->print_mode_fail()) {
      print_scenario_file_fail(x);
    }
  }

  // Task is defined by (sub-)domain and index of objective
  // Computation of objective, gradient of obejctive
  // in interval arithemtic
  //
  // Elimination of domain that cannot contain global minimum
  // or refinement of domain if it is not fine enough
  void task(const vector<interval_type>& x, const vector<float_type> &v) {
    increment_counter(0);
#pragma omp master
    {
      _solver_status->master_cnt()++;
      if (_solver_status->master_cnt()%(_solver_config->print_status_every()/64)==0 && !_solver_config->timing_mode())
        _solver_status->print();
    }
    bitset<3> status(0);
    vector<bool> x_mask(x.size(), true);
    interval_type y;
    float_type y_float;
    vector<interval_type> dfdx(x.size(),interval_type::whole());
    bool converged = false;
    bool locally_convex = false;

    for (size_t i=0; i<x.size(); i++) {
      if (width(x[i])<=_solver_config->eps_x() || (dfdx[i].lower()==0 && dfdx[i].upper()==0)) {
        x_mask[i] = false;
      }
    }

    try {
      value_check(status,x,x_mask,v,y,y_float);
      if (_solver_config->gradient_check()==stationary_only) status.reset(0);
      // Check if the value range can contain a global minimum
//      if (!_solver_config->print_mode_ns() && status.any()) return;
      float_type y_dist = abs(y.lower()-y_float);
      if (y_dist<_solver_config->eps_y()) {
        converged = true;
      }
    } catch (boost::numeric::interval_lib::comparison_error&) {
      exception(x,x_mask,v);
    }

    if (_solver_config->gradient_check()!=grad_check_off) {
      try {
        gradient_check(status,x,x_mask,v,dfdx);
        if (!_solver_config->print_mode_ns() && status.any()) return;
      } catch (boost::numeric::interval_lib::comparison_error&) {
        exception(x,x_mask,v);
      }
    }

    if (_solver_config->hessian_check()) {
      try {
        interval_type det=0;
        hessian_check(status,x,v,det);
        // TODO convexity check for x.size()>1
        if (x.size()<=3 && det.lower() > 0) {
          locally_convex = true;
        }
        if (!_solver_config->print_mode_ns() && status.any()) return;
      } catch (boost::numeric::interval_lib::comparison_error&) {
        exception(x,x_mask,v);
      }
    }

    if (status.any()) {
      if (_solver_config->print_mode_ns()) {
        print_scenario_file(status.to_ulong()+1,x,dfdx,y,"noglmin");
      }
      return;
    }
    assert(status.none());

    if (locally_convex) {
      vector<interval_type> x_pair(x.size());
      for (size_t i=0; i<x.size(); i++) {
        x_pair[i] = interval_type(x[i].lower(),x[i].upper());
      }
#pragma omp critical
      _minima.push_back(x_pair);
    } else {
      bool task_generated = false;
      if (!converged) {
        task_generated = branching(x,x_mask,v);
      }

      if (converged || !task_generated) {
        if (converged) {
          increment_counter(5);
        } else {
          increment_counter(6);
        }
        if (_solver_config->print_mode()) {
          print_scenario_file(1,x,dfdx,y,"glmin");
        }
      }
    }
  }
};
