#pragma once
#include <vector>
#include <cfloat>
#include <limits>
#include <string>
using namespace std;

enum global_e {
  interval_natural,
  interval_meanvalue,
  interval_hybrid,
  mccormick,
  mccormick_hybrid,
  best
};

enum value_check_e {
  val_check_off,
  upperbound,
  midpoint,
  gradient_descent_3,
  gradient_descent_10,
  gradient_descent_log
};

enum grad_check_e {
  grad_check_off,
  stationary_and_bound,
  stationary_only
};

template<typename interval, typename float_type>
class solver_config {
  bitset<3> _print_mode = 0;
  bool _timing_mode = false;
  global_e _globalization_y = interval_natural;
  global_e _globalization_dfdx = interval_natural;
  bool _hesc = false;
  size_t _ntime = 1;
  value_check_e _valc = midpoint;
  grad_check_e _gradc = stationary_and_bound;
  size_t _print_status_every=100000000;

  float_type _width_hybrid = 1.0;
  size_t _mcpp_max_iter = 20;
  float_type _epsx = numeric_limits<float_type>::epsilon();
  float_type _epsy = sqrt(numeric_limits<float_type>::epsilon());
  float_type _epsdfdx = sqrt(numeric_limits<float_type>::epsilon());
  float_type _epsns = 1e-4;
  float_type _epsmcpp = sqrt(numeric_limits<float_type>::epsilon());

  vector<interval> _domain;
  vector<float_type> _param;

  void read_argv(int argc, char* argv[]) {
    int cnt=1;
    if (argc >= cnt+1) _print_mode = stoi(argv[cnt]);
    cnt++;
    if (argc >= cnt+1 && stoi(argv[cnt])>0) {
      _timing_mode = true;
      _ntime = stoi(argv[cnt]);
    }
    cnt++;
    if (argc >= cnt+1) {
      switch(stoi(argv[cnt])) {
      case 0: _globalization_y = interval_natural; break;
      case 1: _globalization_y = interval_meanvalue; break;
      case 2: _globalization_y = interval_hybrid; break;
      case 3: _globalization_y = mccormick; break;
      case 4: _globalization_y = mccormick_hybrid; break;
      case 5: _globalization_y = best; break;
      default: _globalization_y = interval_natural; break;
      }
    }
    cnt++;
    if (argc >= cnt+1) {
      switch(stoi(argv[cnt])) {
      case 0: _globalization_dfdx = interval_natural; break;
      case 1: _globalization_dfdx = interval_meanvalue; break;
      case 2: _globalization_dfdx = interval_hybrid; break;
      case 3: _globalization_dfdx = mccormick; break;
      case 4: _globalization_dfdx = mccormick_hybrid; break;
      case 5: _globalization_dfdx = best; break;
      default: _globalization_dfdx = interval_natural; break;
      }
    }
    cnt++;
    if (argc >= cnt+1) {
      switch(stoi(argv[cnt])) {
      case 0: _valc = val_check_off; break;
      case 1: _valc = upperbound; break;
      case 2: _valc = midpoint; break;
      case 3: _valc = gradient_descent_3; break;
      case 4: _valc = gradient_descent_10; break;
      case 5: _valc = gradient_descent_log; break;
      default: _valc = midpoint; break;
      }
    }
    cnt++;
    if (argc >= cnt+1) set_grad_check(stoi(argv[cnt]));
    cnt++;
    if (argc >= cnt+1) _hesc = stoi(argv[cnt]);
    cnt++;
    if (argc >= cnt+1) _epsx = stod(argv[cnt])<0?sqrt(numeric_limits<float_type>::epsilon()):stod(argv[cnt]);
    cnt++;
    if (argc >= cnt+1) _epsy = stod(argv[cnt])<0?sqrt(numeric_limits<float_type>::epsilon()):stod(argv[cnt]);
    cnt++;
    if (argc >= cnt+1) _epsdfdx = stod(argv[cnt])<0?sqrt(numeric_limits<float_type>::epsilon()):stod(argv[cnt]);
    cnt++;
    if (argc >= cnt+1) _epsns = stod(argv[cnt])<0?sqrt(numeric_limits<float_type>::epsilon()):stod(argv[cnt]);
    cnt++;
  }

public:
  solver_config(int argc, char* argv[], vector<interval>& domain, vector<float_type>& param) : _domain(domain), _param(param) {
    read_argv(argc,argv);
  }

  solver_config(int argc, char* argv[], vector<interval>& domain) : _domain(domain), _param() {
    read_argv(argc,argv);
  }

  void print() {
    cout << " PRINT_MODE = " << _print_mode.test(0) << endl;
    cout << " PRINT_NOGL_MODE = " << _print_mode.test(1) << endl;
    cout << " PRINT_FAIL_MODE = " << _print_mode.test(2) << endl;
    cout << " GLOBAL_METHOD for y: ";
    switch (_globalization_y) {
    case interval_natural: cout << "NIE" << endl; break;
    case interval_meanvalue: cout << "MVF" << endl; break;
    case interval_hybrid: cout << "hybrid NIE + MVF, switch at width: " << _width_hybrid << endl; break;
    case mccormick: cout << "MCR, MCR iter: " << _mcpp_max_iter << endl; break;
    case mccormick_hybrid: cout << "NIE + MCR, MCR iter: " << _mcpp_max_iter << ", switch at width: " << _width_hybrid << endl; break;
    case best: cout << "best NIE + MVF + MCR, MCR iter: " << _mcpp_max_iter << endl; break;
    default: cout << "unkown" << endl; break;
    }
    cout << " GLOBAL_METHOD for dfdx: ";
    switch (_globalization_dfdx) {
    case interval_natural: cout << "NIE" << endl; break;
    case interval_meanvalue: cout << "MVF" << endl; break;
    case interval_hybrid: cout << "hybrid NIE + MVF, switch at width: " << _width_hybrid << endl; break;
    case mccormick: cout << "MCR, MCR iter: " << _mcpp_max_iter << endl; break;
    case mccormick_hybrid: cout << "NIE + MCR, MCR iter: " << _mcpp_max_iter << ", switch at width: " << _width_hybrid << endl; break;
    case best: cout << "best NIE + MVF + MCR, MCR iter: " << _mcpp_max_iter << endl; break;
    default: cout << "unkown" << endl; break;
    }
    cout << " VALUE_CHECK = " << _valc << endl;
    cout << " GRADIENT_CHECK = " << _gradc << endl;
    cout << " HESSIAN_CHECK = " << _hesc << endl;
    cout << " N = " << _domain.size() << endl;
    cout << " EPS_X = " << _epsx << endl;
    cout << " EPS_Y = " << _epsy << endl;
    cout << " EPS_DFDX = " << _epsdfdx << endl;
    cout << " EPS_NS = " << _epsns << endl;
    cout << " EPS_MCPP = " << _epsmcpp << endl;
#pragma omp parallel
#pragma omp single
    {
      cout << " THREADS = " << omp_get_num_threads() << endl;
    }
  }

  const bool& timing_mode() { return _timing_mode; }
  const size_t& ntime() { return _ntime; }
  bool print_mode() { return _print_mode.test(0); }
  bool print_mode_ns() { return _print_mode.test(1); }
  bool print_mode_fail() { return _print_mode.test(2); }
  const value_check_e& value_check() { return _valc; }
  const grad_check_e& gradient_check() { return _gradc; }
  const bool& hessian_check() { return _hesc; }
  const global_e& globalization_y() { return _globalization_y; }
  const global_e& globalization_dfdx() { return _globalization_dfdx; }
  const size_t& print_status_every() { return _print_status_every; }
  const float_type& width_hybrid() { return _width_hybrid; }
  const float_type& eps_x() { return _epsx; }
  const float_type& eps_y() { return _epsy; }
  const float_type& eps_dfdx() { return _epsdfdx; }
  const float_type& eps_ns() { return _epsns; }
  const float_type& eps_mcpp() { return _epsmcpp; }
  const size_t& mcpp_max_iter() { return _mcpp_max_iter; }
  const vector<interval>& domain() { return _domain; }
  const vector<float_type>& param() { return _param; }
  void set_domain(vector<interval>& domain) { _domain = domain; }
  void set_param(vector<float_type>& param) { _param = param; }
  void set_val_check() { _valc = midpoint; }
  void unset_val_check() { _valc = val_check_off; }
  //void set_grad_check() { _gradc = stationary_and_bound; }
  void set_grad_check(int c) {
    switch(c) {
        case 0: _gradc = grad_check_off; break;
        default:
        case 1: _gradc = stationary_and_bound; break;
        case 2: _gradc = stationary_only; break;
    }
  }
  void unset_grad_check() { _gradc = grad_check_off; }
  void set_hes_check() { _hesc = true; }
  void unset_hes_check() { _hesc = false; }
};

template<typename float_type>
class solver_status {
  int _start_time;
  unsigned long long _master_cnt;
  vector<unsigned long long> _counter;
  vector<float_type> _global_min_bound;
  unsigned long long _tapesize=0;
public:
  solver_status() : _master_cnt(0), _counter(vector<unsigned long long>(11,0.0)) {
    _start_time = omp_get_wtime();
    _global_min_bound.resize(1, numeric_limits<float_type>::max());
  }
  void print() {
    stringstream stream;
    stream << _counter[0] << " MIN=";
    for (auto& gmin : _global_min_bound)
      stream << gmin << " ";
    //stream << _global_min_bound[0];
    stream << "\t";
    stream << " V=" << _counter[1];
    stream << " G=" << _counter[2];
    stream << " H=" << _counter[3];
    stream << " NS=" << _counter[4];
    stream << " OPTc=" << _counter[5];
    stream << " OPTb=" << _counter[6];
    stream << " R=" << _counter[7];
    stream << " S=" << _counter[8];
    stream << " XV=" << _counter[9];
    stream << " XD=" << _counter[10];
    stream << " T(sec)=" << omp_get_wtime()-_start_time;
    stream << endl;
    cout << stream.rdbuf();
  }
  void print_final() {
    print();
    stringstream stream;
    stream << "global minimum bound is " << _global_min_bound[0] << endl;
    stream << "total         \t" << _counter[0] << endl;
    stream << "refined       \t" << _counter[7] << "\t" << _counter[7]/static_cast<float>(_counter[0]) << endl;
    stream << "non smooth    \t" << _counter[4] << "\t" << _counter[4]/static_cast<float>(_counter[0]) << endl;
    stream << "converged     \t" << _counter[5] << "\t" << _counter[5]/static_cast<float>(_counter[0]) << endl;
    stream << "not converged \t" << _counter[6] << "\t" << _counter[6]/static_cast<float>(_counter[0]) << endl;
    stream << "exact gradient\t" << _counter[10] << "\t" << _counter[10]/static_cast<float>(_counter[0]) << endl;
    cout << stream.rdbuf();
  }
  const int& start_time() { return _start_time; }
  unsigned long long& master_cnt() { return _master_cnt; }
  vector<unsigned long long>& counter() { return _counter; }
  unsigned long long& counter(size_t i) { return _counter[i]; }
  vector<float_type>& global_min_bound() { return _global_min_bound; }
  float_type& global_min_bound(size_t i) { return _global_min_bound[i]; }
  unsigned long long& tapesize() { return _tapesize; }
  void set_tapesize(const unsigned long long& ts) { _tapesize = ts; }
};

