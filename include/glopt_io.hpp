#pragma once
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "omp.h"
#include "glopt_solver_config.hpp"
using namespace std;

template<typename T>
inline void print_scenario_file(int result, const vector<T>& x, const vector<T>& dx, const T& y, string str="opt") {
  stringstream stream;
  stream << setprecision(15) << result << " ";
  for (auto const& xi : x) {
    stream << xi.lower() << " " << xi.upper() << " ";
  }
  stream << y.lower() << " " << y.upper() << " ";
  for (auto const& dxi : dx) {
    stream << dxi.lower() << " " << dxi.upper() << " ";
  }
  stream << "\n";
  stringstream filename;
  filename << str << omp_get_thread_num() << ".txt";
  //if (upper(dco::value(dco::value(y))) <= MIN_UPPER_BOUND) {
  std::fstream file(filename.str(), std::fstream::out | std::fstream::app);
  file << stream.rdbuf();
  file.close();
  //}
}

template<typename T>
inline void print_scenario_file(int result, const vector<T>& x, const T& y, string str="opt") {
  stringstream stream;
  stream << setprecision(15) << result << " ";
  for (auto const& xi : x) {
    stream << dco::value(dco::value(xi)).lower() << " " << dco::value(dco::value(xi)).upper() << " ";
  }
  stream << dco::value(dco::value(y)).lower() << " " << dco::value(dco::value(y)).upper() << " ";
  for (auto const& xi : x) {
    stream << dco::value(dco::derivative(xi)).lower() << " " << dco::value(dco::derivative(xi)).upper() << " ";
  }
  stream << "\n";
  stringstream filename;
  filename << str << omp_get_thread_num() << ".txt";
  //if (upper(dco::value(dco::value(y))) <= MIN_UPPER_BOUND) {
  std::fstream file(filename.str(), std::fstream::out | std::fstream::app);
  file << stream.rdbuf();
  file.close();
  //}
}

template<typename T>
inline void print_scenario_file_fail(const vector<T>& x, string str="fail") {
  size_t result = 0;
  stringstream stream;
  stream << setprecision(7) << result << " ";
  for (auto const& xi : x) {
    stream << dco::value(xi).lower() << " " << dco::value(xi).upper() << " ";
  }
  stream << "\n";
  stringstream filename;
  filename << str << omp_get_thread_num() << ".txt";
  std::fstream file(filename.str(), std::fstream::out | std::fstream::app);
  file << stream.rdbuf();
  file.close();
}

template<typename T>
inline void postprocessing_file(const size_t proc, const T& opt, const size_t n, string str="opt") {
  stringstream filename_pp;
  filename_pp << str << proc << "_pp.txt";
  std::fstream file_out(filename_pp.str(), std::fstream::out);

  stringstream filename;
  filename << str << proc << ".txt";
  ifstream file_in(filename.str());
  string line;
  size_t i_ylower= 2*n+1;;
  T y_lower;
  while(getline(file_in, line)) {
    stringstream ss(line);
    string tmp;
    for (size_t i=0; i<= i_ylower; i++) {
      ss >> tmp;
    }
    y_lower = stod(tmp);
    if (y_lower <= opt) {
      file_out << line << "\n";
    }
  }
  file_out.close();
}
